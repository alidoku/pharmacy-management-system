package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import Model.worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class addpunetoreController implements Initializable{

	Database db = new Database();
	Service service = new Service();
	@FXML
	private TextField emri,mbiemri,username,address,email,tel;
	
	@FXML
	private PasswordField pass;
	
	@FXML
	private DatePicker data;
	
	@FXML
	private Button pastro,ruaj;
	
	@FXML
	private TextArea shenim;
	
	@FXML
	private CheckBox status;
	
	
	@FXML
	private void PastroAction(ActionEvent event)
	{
		emri.setText("");
		mbiemri.setText("");
		username.setText("");
		pass.setText("");
		address.setText("");
		email.setText("");
		tel.setText("");
		shenim.setText("");
	}
	
	@FXML
	private void RuajAction(ActionEvent event)
	{
		 String sql = "INSERT INTO `users`(`id`, `name`, `surname`, `address`, `dob`,`email`,`phone_num`, `status`, `username`, `password`, `type`,`description`)"+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		 Connection conn = db.connect();
		 
		 if(emri.getText().isEmpty() ||mbiemri.getText().isEmpty())
			{
				ruaj.disabledProperty();
				Dialogs.create()
				.title("MESAZHI")
				.message("Ju lutem plotesojini te gjithe fushat !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showWarning();
				return;
			}
		 
		 int id = 0;
		 String sts="";
		 
		 if(status.isSelected())
		 {
			 sts = status.getText();
		 }
		 else 
			 sts = "PASIV";
		 
		 java.sql.Date sqlDate = java.sql.Date.valueOf(data.getValue());
		 
		 
		 
		 
		 worker work = new worker(emri.getText(),mbiemri.getText(),address.getText(),data.getValue().toString(),email.getText(),tel.getText(),username.getText(),pass.getText(),sts,shenim.getText());
	
		 try {
			PreparedStatement pst = (PreparedStatement)conn.prepareStatement(sql);
			pst.setInt(1, id);
			pst.setString(2, work.getName());
			pst.setString(3, work.getSurname());
			pst.setString(4, work.getAddress());
			pst.setDate(5, sqlDate);
			pst.setString(6, work.getEmail());
			pst.setString(7, work.getNr_cel());
			pst.setString(8, work.getStatus());
			pst.setString(9, work.getUsername());
			pst.setString(10, work.getPass());
			pst.setString(11, "WORKER");
			pst.setString(12, work.getShenime());
			
			pst.execute();
			 pst.close();
			 conn.close();
			 Dialogs.create()
				.title("MESAZHI")
				.message("Punetori u ruajt me sukses !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showInformation();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 close();
		 
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		tel.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(10));
		emri.addEventFilter(KeyEvent.KEY_TYPED, service.letter_Validation(100));
		mbiemri.addEventFilter(KeyEvent.KEY_TYPED, service.letter_Validation(100));
		
	}
	
	private void close()
	{
		Stage stage = (Stage) ruaj.getScene().getWindow();
		
		stage.close();
	}

}
