package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;
import com.sun.corba.se.spi.orbutil.fsm.Action;

import Model.Bills;
import Model.Invoice;
import Model.article;
import Model.client;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Skin;
import javafx.scene.control.Skinnable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.DoubleStringConverter;



public class posSystemController implements Initializable{

	Database db = new Database();
	Service service = new Service();
	private int client_id;
	ObservableList<article> articles = FXCollections.observableArrayList();
	
	@FXML
	private TableView<article> table;
	
	@FXML
	private TableColumn<article, Integer> id;
	@FXML
	private TableColumn<article, String> kodi;
	@FXML
	private TableColumn<article, String> pershkrimi;
	@FXML
	private TableColumn<article, String> njesia;
	@FXML
	private TableColumn<article, Double> sasia;
	@FXML
	private TableColumn<article, Double> shitje;
	@FXML
	private TableColumn<article, Double> totali;
	
	@FXML
	private TextField sipas_kodi, sipas_emrit, vendos_sasi;
	
	@FXML
	private Label klienti,tot,kerko_emer, kerko_kod;
	
	@FXML
	private Button ndrysho_klient, faturo,  hiq_article, sipas_persh, sipas_kodi12, fut_recete;
	
	String emeruser = "";
	int id_user  = 0;
	boolean found = false;
	@FXML
	public void onEnter2()
	{
		found = false;
		String kod = sipas_emrit.getText();
		   article a = SearchName(kod); 
		   
		   if(found)
		   {
			   if(vendos_sasi.getText().isEmpty()){
				   if(a.getKompesim().equals("PO") && a.getKategoria().equals(service.kategori)){
					   Optional<String> response = Dialogs.create()
						        .title("Vendos Sasine")
						        .message("Vendosni sasine e deshiruar:")
						        .showTextInput("");
					   		double sasi = 0;
						// One way to get the response value.
						if (response.isPresent()) {
							  sasi = Double.parseDouble(response.get());
						}
					  
					   a.setSasia(sasi);
					   a.setTotali_artikull(0.0);
					   articles.add(a); 
					   sipas_emrit.setText("");
					   vendos_sasi.setText("");
				   }else{
					   Optional<String> response = Dialogs.create()
						        .title("Vendosni sasine")
						        .message("Vendosni sasine e deshiruar:")
						        .showTextInput("");
					   		double sasia = 0;
						// One way to get the response value.
						if (response.isPresent()) {
							  sasia = Double.parseDouble(response.get());
						}
					  
					   //double sasi = Double.parseDouble(sasia);
					   a.setSasia(sasia);
					   double cmimi = a.getPriceout();
					   a.setTotali_artikull(sasia * cmimi);
					   articles.add(a); 
					   sipas_emrit.setText("");
					   vendos_sasi.setText("");
				   }
			   }
			   else{
				   if(a.getKompesim().equals("PO") && a.getKategoria().equals(service.kategori)){
					   double sasi = Double.parseDouble(vendos_sasi.getText());
					   a.setSasia(sasi);
					   double cmimi = a.getPriceout();
					   a.setTotali_artikull(0.0);
					    
					   articles.add(a); 
					   sipas_emrit.setText("");
					   vendos_sasi.setText("");
				   }else{
					   double sasi = Double.parseDouble(vendos_sasi.getText());
					   a.setSasia(sasi);
					   double cmimi = a.getPriceout();
					   a.setTotali_artikull(sasi * cmimi);
					    
					   articles.add(a); 
					   sipas_emrit.setText("");
					   vendos_sasi.setText("");
				   }
			   }
			  
		   }
		   else
		   {
			   Dialogs.create()
				.title("MESAZHI")
				.message("Artikulli nuk u gjend !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showWarning();
			   
			   sipas_kodi.setText("");
		   }
		}
	@FXML
	public void onEnter(){

		  
		   String kod = sipas_kodi.getText();
		   article a = Search(kod); int i = 0;
		   System.out.println(a.toString()) ;
		   if(found)
		   {
			   if(vendos_sasi.getText().isEmpty()){
				   if(a.getKompesim().equals("PO") && a.getKategoria().equals(service.kategori)){
					   Optional<String> response = Dialogs.create()
						        .title("Vendosni sasine")
						        .message("Vendosni sasine e deshiruar:")
						        .showTextInput("");
					   		double sasi = 0;
						// One way to get the response value.
						if (response.isPresent()) {
							  sasi = Double.parseDouble(response.get());
						}
					   a.setSasia(sasi);
					   a.setTotali_artikull(0.0);
					   articles.add(a); 
					   sipas_emrit.setText("");
					   vendos_sasi.setText("");
				   }else{
					   Optional<String> response = Dialogs.create()
						        .title("Vendosni sasine")
						        .message("Vendosni sasine e deshiruar:")
						        .showTextInput("");
					   		double sasi = 0;
						// One way to get the response value.
						if (response.isPresent()) {
							  sasi = Double.parseDouble(response.get());
						}
					   a.setSasia(sasi);
					   double cmimi = a.getPriceout();
					   a.setTotali_artikull(sasi * cmimi);
					    
					   articles.add(a); 
					   sipas_kodi.setText("");
					   vendos_sasi.setText("");
				   }
			   }
			   else{
				   if(a.getKompesim().equals("PO") && a.getKategoria().equals(service.kategori)){
					   double sasi = Double.parseDouble(vendos_sasi.getText());
					   a.setSasia(sasi);
					   double cmimi = a.getPriceout();
					   a.setTotali_artikull(0.0);
					    
					   articles.add(a); 
					   sipas_kodi.setText("");
					   vendos_sasi.setText("");
				   }else{
					   double sasi = Double.parseDouble(vendos_sasi.getText());
					   a.setSasia(sasi);
					   double cmimi = a.getPriceout();
					   a.setTotali_artikull(sasi * cmimi);
					    
					   articles.add(a); 
					   sipas_kodi.setText("");
					   vendos_sasi.setText("");
				   }
			   }
			  
		   }
		   else
		   {
			   Dialogs.create()
				.title("MESAZHI")
				.message("Artikulli nuk u gjend !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showWarning();
			   sipas_kodi.setText("");
		   }
		}
	public article SearchName(String name)
	{
		String sql = "select * from articles where description='"+name + "' and status='AKTIV'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		found = false;
		article a = new article();
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				a.setId(rs.getInt(1));
				a.setKodi1(rs.getString(2)); 
				a.setKodi2(rs.getString(3)); 
				a.setPershkrimi(rs.getString(4)); 
				a.setNjesia(rs.getString(5));
				a.setSasia(1);
				a.setPriceout(rs.getDouble(12));
				String nam = service.getkategoria(rs.getInt(7));
				a.setKategoria(nam);
				a.setKompesim(rs.getString(8));
				found = true;
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return a;
	}
	public article Search(String kodi)
	{
		String sql = "select * from articles where kodi_1='"+kodi + "' or kodi_2='"+kodi + "' and status='AKTIV'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		found = false;
		article a = new article();
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				a.setId(rs.getInt(1));
				a.setKodi1(rs.getString(2)); 
				a.setKodi2(rs.getString(3)); 
				a.setPershkrimi(rs.getString(4)); 
				a.setNjesia(rs.getString(5));
				a.setSasia(1);
				a.setPriceout(rs.getDouble(12));
				String nam = service.getkategoria(rs.getInt(7));
				a.setKategoria(nam);
				a.setKompesim(rs.getString(8));
				found = true;
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return a;
	}
	public void getUser(int id,String name,String surname)
	{
		emeruser = name + " " + surname;
		id_user = id;
	}
	@FXML
	public void NdryshoKlient(ActionEvent event)
	{
		Optional<String> response = Dialogs.create()
		        .title("Vendosni Klientin")
		        .message("Vendosni emrin e klientit:")
		        .showTextInput("");
	   		String klient = "";
		// One way to get the response value.
		if (response.isPresent()) {
			klient = response.get();
		}
		System.out.println(klient);
		boolean found = service.findKlient(klient);
		client_id = service.id;
		if(found)
			klienti.setText(service.namesurname);
		else{
			Dialogs.create()
			.title("MESAZHI")
			.message("Klienti nuk u gjend !!! ")
			.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
			.showWarning();
		}
	}
	@FXML
	public void HiqArtikull(ActionEvent event)
	{
		article a = table.getSelectionModel().getSelectedItem();
		articles.remove(a);
	}
	@FXML
	public void SipasPershkrimit(ActionEvent event)
	{
		sipas_emrit.setVisible(true);
		kerko_emer.setVisible(true);
		sipas_emrit.requestFocus();
		sipas_kodi.setVisible(false);
		kerko_kod.setVisible(false);
		
		System.out.println("Presssed Pershkrimi");
	}
	@FXML
	public void SipasKodit(ActionEvent event)
	{
		sipas_emrit.setVisible(false);
		kerko_emer.setVisible(false);
		sipas_kodi.setVisible(true);
		kerko_kod.setVisible(true);
		sipas_kodi.requestFocus();
		
		
		System.out.println("Presssed Kodi");
	}
	@FXML
	public void Totali(MouseEvent event)
	{
		double shuma = 0,sh;
		ArrayList<Double> columnData = new ArrayList<Double>(table.getItems().size());
		for(article item : table.getItems())
		{
			columnData.add(totali.getCellObservableValue(item).getValue());
			
		}
		for(int i = 0; i < columnData.size(); i++)
		{
			shuma += columnData.get(i);
		}
	
		tot.setText(String.format( "%.2f", shuma));
		System.out.println(tot.getText());
	}
	public int FaturaId()
	{
		String sql = "SELECT  *  FROM fatura";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		int result = 0;
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next())
				result = rs.getInt(1);
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		
	}
	
	@FXML
	public void Faturo(ActionEvent event)
	{
		String sql = "INSERT INTO `fatura`(`id`, `emer`, `Tipi`, `worker_id`,`client_id`,`total`,`status`,`create_at`)"+ " VALUES (?,?,?,?,?,?,?,?)";
		String sql1 = "INSERT INTO `shitje`(`id`, `artikull_id`, `fatura_id`, `klient_id`,`sasia`,`cmimi`,`totali`)"+ " VALUES (?,?,?,?,?,?,?)";
		Connection conn = db.connect();
		int fatura_id = 0;
		int klient_id = 0;
		
		
		Bills f= new Bills(fatura_id,"FATURESHITJE","FATURESHITJE",id_user,1,Double.parseDouble(tot.getText()), "PRERE");
		if(client_id != 0) klient_id = client_id;
		else klient_id = 1;
		LocalDate date = LocalDate.now();
		f.setClient_id(klient_id);
		 try {
				PreparedStatement pst = (PreparedStatement)conn.prepareStatement(sql);
				pst.setInt(1, f.getId());
				pst.setString(2, f.getEmer());
				pst.setString(3, f.getTipi());
				pst.setInt(4, f.getWorker_id());
				pst.setInt(5, klient_id);
				pst.setDouble(6, f.getTotal());
				pst.setString(7, f.getStatus());
				pst.setDate(8, java.sql.Date.valueOf(date));
				PreparedStatement pst1 = (PreparedStatement) conn.prepareStatement(sql1);
				
				for(int i = 0; i < articles.size();i++)
				{
					article a = articles.get(i);
					pst1.setInt(1, fatura_id);
					pst1.setInt(2, a.getId());
					int cnt = FaturaId();
					pst1.setInt(3, cnt+1);
					pst1.setInt(4, klient_id);
					pst1.setDouble(5, a.getSasia());
					pst1.setDouble(6, a.getPriceout());
					pst1.setDouble(7, a.getTotali_artikull());
					
					pst1.addBatch();
					
				}
				
				org.controlsfx.control.action.Action ad = Dialogs.create()
							.title("Paguaj Faturen")
							.actions(org.controlsfx.dialog.Dialog.ACTION_OK, org.controlsfx.dialog.Dialog.ACTION_NO)
							.message("Doni te Paguani Kete Fature \n" + " Totali:" + tot.getText())
							.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
							.showConfirm();
							
				
				
				if(ad == org.controlsfx.dialog.Dialog.ACTION_OK){
					pst.execute();
					 pst.close();
					 pst1.executeBatch();
						pst1.close();
						conn.close();
					 table.getItems().removeAll(articles);
					 Dialogs.create()
								.title("MESAZHI")
								.actions(org.controlsfx.dialog.Dialog.ACTION_CLOSE)
								.message("Fatura u Ruaj me Sukses ")
								.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
								.showConfirm();
				}
				else
				{
					table.getItems().removeAll(articles);
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 System.out.println(f.getId());
	}
	
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		table.setEditable(true);
		sipas_emrit.setVisible(false);
		kerko_emer.setVisible(false);
		tot.setText(Integer.toString(0));
		klienti.setText("Kliente te ndryshem");
		id.setCellValueFactory(new PropertyValueFactory<article, Integer>("id"));
		kodi.setCellValueFactory(new PropertyValueFactory<article, String>("kodi1"));	
		pershkrimi.setCellValueFactory(new PropertyValueFactory<article, String>("pershkrimi"));
		njesia.setCellValueFactory(new PropertyValueFactory<article, String>("njesia"));
		sasia.setCellValueFactory(new PropertyValueFactory<article, Double>("sasia"));
		shitje.setCellValueFactory(new PropertyValueFactory<article, Double>("priceout"));
		totali.setCellValueFactory(new PropertyValueFactory<article, Double>("totali_artikull"));
		table.setItems(articles);
		
		Callback<TableColumn<article, Double>, TableCell<article, Double>> defaultTextFieldCellFactory 
        = TextFieldTableCell.<article,Double>forTableColumn(new DoubleStringConverter());
		
		sasia.setCellFactory(col -> {
            TableCell<article, Double> cell  = defaultTextFieldCellFactory.call(sasia);
          
            cell.itemProperty().addListener((obs, oldValue, newValue) -> {
                TableRow row = cell.getTableRow();
                if (row == null) {
                    cell.setEditable(false);
                } else {
                    article item = (article) cell.getTableRow().getItem();
                    if (item == null) {
                        cell.setEditable(false);
                    } else {
                        cell.setEditable(item.getSasia() % 2 == 0);
                    }
                }
               cell.setEditable(true);
            });
            return (TableCell<article, Double>) cell ;
        });
		
		fut_recete.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/View/AddRecete.fxml").openStream());
				Recete_Controller controller = (Recete_Controller)loader.getController();
				if(!klienti.getText().isEmpty() && !klienti.getText().equals("Kliente te ndryshem")){
					controller.getKlient(klienti.getText());
					controller.getClientId(client_id);
				}
				else
				{
					controller.getKlient(klienti.getText());
					controller.getClientId(1);
				}
				Scene scene = new Scene(root,1000,600);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Krijo Artikull");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		sipas_emrit.addEventFilter(KeyEvent.KEY_TYPED, service.letter_Validation(20));
		sipas_kodi.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(20));
		vendos_sasi.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(5));
		
	}
	
}
