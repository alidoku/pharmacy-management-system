package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import Model.Bills;
import Model.client;
import Model.worker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Menaxho_Faturat_Controller implements Initializable{

	Database db = new Database();
	Service service = new Service();
	public int id_kot;
	public double id_plot;
	private double sum;
	@FXML
	private TableView<Bills> table;
	
	@FXML
	private TableColumn<Bills, Integer> id;
	
	@FXML
	private TableColumn<Bills, String> emri_fatura;
	
	@FXML
	private TableColumn<Bills, String> fatura;
	
	@FXML
	private TableColumn<Bills, Integer> worker;
	
	@FXML
	private TableColumn<Bills, Integer> klienti;
	
	@FXML
	private TableColumn<Bills, Double> totali;
	
	@FXML
	private TableColumn<Bills, Date> data_shitje;
	
	@FXML
	private TableColumn<Bills, String> status;
	
	@FXML
	private DatePicker nga,deri;
	
	@FXML
	private ComboBox punetori;
	
	@FXML
	private Label tot;
	
	@FXML
	private Button kerko, shiko, anullo;
	
	@FXML
	public void MouseEvent(javafx.scene.input.MouseEvent event)
	{
		if(event.isPrimaryButtonDown() && event.getClickCount() == 2){
			Bills bills = table.getSelectionModel().getSelectedItem();
			id_kot = bills.getId();
			id_plot = bills.getTotal();
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Shiko_fature.fxml").openStream());
				Shiko_Fature_Controller controller = (Shiko_Fature_Controller)loader.getController();
				controller.getTable(id_kot, id_plot);
				Scene scene = new Scene(root,1300,600);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Shiko Faturat");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			
			
		}
	
		
	}
	@FXML
	private void AnulloEvent(ActionEvent event)
	{
		Bills in = table.getSelectionModel().getSelectedItem();
		int id = in.getId();
		String sql = "UPDATE `fatura` SET `status`='ANULLUAR' WHERE id='"+id+"'";
		Connection conn = db.connect();
		Statement smt;
		
		try {
			smt = conn.createStatement();
			smt.executeUpdate(sql);
			
			Dialogs.create()
			.title("MESAZHI")
			.message("Fatura u Anullua me sukses !!! ")
			.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
			.showInformation();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		table.getItems().clear();
		table.setItems(getFatura());
		
	}
	@FXML
	private void ShikoEvent(ActionEvent event)
	{
		Bills bills = table.getSelectionModel().getSelectedItem();
		System.out.println(bills.toString());
		id_kot = bills.getId();
		id_plot = bills.getTotal();
		
		try {
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			AnchorPane  root = loader.load(getClass().getResource("/View/Shiko_fature.fxml").openStream());
			Shiko_Fature_Controller controller = (Shiko_Fature_Controller)loader.getController();
			controller.getTable(bills.getId(), bills.getTotal());
			System.out.println(bills.getId() + " " + bills.getTotal());
			Scene scene = new Scene(root,1100,600);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Shiko Faturat");
			primaryStage.show();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	public ObservableList<Bills> getFatura1()
	{
		ObservableList<Bills> bills = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		Bills w;
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		
		String work =  punetori.getSelectionModel().getSelectedItem().toString();
		int worker_id = service.getworker(work);
		String sql = "";
		if(punetori.getSelectionModel().isEmpty() ){
		 sql = "select * from fatura where create_at between '"+ nga.getValue() +"' and '" + deri.getValue() +"' and fatura.Tipi='FATURESHITJE'";
		}else
		{
			 sql = "select * from fatura where create_at between '"+ nga.getValue() +"' and '" + deri.getValue() +"' and worker_id='"+worker_id+"' and fatura.Tipi='FATURESHITJE'";
		}
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				
				w = new Bills(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4),rs.getInt(5),rs.getDouble(6), rs.getString(7));
				w.setCreated_at(df.format(rs.getDate(8)));
				sum += w.getTotal();
				bills.add(w);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bills;
	}
	
	public ObservableList<Bills> getFatura()
	{
		ObservableList<Bills> bills = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		Bills w;
		
		
		String sql = "select * from fatura ";
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				w = new Bills(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4),rs.getInt(5),rs.getDouble(6), rs.getString(7));
				w.setCreated_at(df.format(rs.getDate(8)));
				sum += w.getTotal();
				bills.add(w);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bills;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		id.setCellValueFactory(new PropertyValueFactory<Bills, Integer>("id"));
		emri_fatura.setCellValueFactory(new PropertyValueFactory<Bills, String>("emer"));
		fatura.setCellValueFactory(new PropertyValueFactory<Bills, String>("tipi"));
		worker.setCellValueFactory(new PropertyValueFactory<Bills, Integer>("worker_id"));
		klienti.setCellValueFactory(new PropertyValueFactory<Bills, Integer>("client_id"));
		totali.setCellValueFactory(new PropertyValueFactory<Bills, Double>("total"));
		data_shitje.setCellValueFactory(new PropertyValueFactory<Bills, Date>("created_at"));
		status.setCellValueFactory(new PropertyValueFactory<Bills, String>("status"));
		
		punetori.setItems(service.getWorker());
		kerko.setOnAction(e->{
			if(punetori.getSelectionModel().isEmpty()){
				table.setItems(getFatura());
				tot.setText(Double.toString(sum));
			}
			else
			{
				table.setItems(getFatura1());
				tot.setText(Double.toString(sum));
			}
		});
		
	}

}
