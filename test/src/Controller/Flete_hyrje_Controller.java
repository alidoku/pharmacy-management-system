package Controller;

import java.net.URL;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import Model.article;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class Flete_hyrje_Controller implements Initializable{

	Database db = new Database();
	Service service = new Service();
	int art_id;
	String n;
	ObservableList<article> articles = FXCollections.observableArrayList();
	
	@FXML
	private TableView<article> table;
	
	@FXML
	private TableColumn<article, Integer>id;
	
	@FXML
	private TableColumn<article, String>kodi;
	
	@FXML
	private TableColumn<article, String>pershkrimi;
	
	@FXML
	private TableColumn<article, String>njesia;
	
	@FXML
	private TableColumn<article, Integer>sasia;
	
	@FXML
	private TableColumn<article, Double>cmimi_blerje;
	
	@FXML
	private TableColumn<article, Double>totali;
	
	@FXML
	private TextField emri, kod, sasi, blerje;
	
	@FXML
	private Button zgjidh,ruaj,hiq,mbyll;
	
	@FXML
	private Label tot;
	
	public int findArtikull(int id)
	{
		String sql = "select * from articles where id='"+id + "'";
		Connection conn = db.connect();
		Statement st;
		ResultSet rs;
		int quantity = 0;
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			if(rs.next())
				quantity = rs.getInt(9);
			else
				quantity = 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return quantity;
	
	}
	public int FaturaId()
	{
		String sql = "SELECT  *  FROM fatura";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		int result = 0;
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next())
				result = rs.getInt(1);
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		
	}
	@FXML
	private void RuajAction(ActionEvent event)
	{
		String sql = "INSERT INTO `fatura`(`id`, `emer`, `Tipi`, `worker_id`,`total`,`status`,`create_at`)"+ " VALUES (?,?,?,?,?,?,?)";
		Connection conn = db.connect();
		Statement st;
		ResultSet rs;
		
		try {
			PreparedStatement pst = (PreparedStatement)conn.prepareStatement(sql);
				LocalDate date = LocalDate.now();
				pst.setInt(1, 0);
				pst.setString(2, "FLETE HYRJE");
				pst.setString(3, "FLETEHYRJE");
				pst.setInt(4, 1);
				pst.setDouble(5, Double.parseDouble(tot.getText()));
				pst.setString(6, "LIKUJDUAR");
				pst.setDate(7,  java.sql.Date.valueOf( date));
				
				
				int cnt = FaturaId();
				for(int i = 0; i < articles.size();i++)
				{
					
					article a = articles.get(i);
					
					int qty = findArtikull(a.getId());
					int qtytotal = (int) (qty + a.getQuantity());
					String sql1 = "UPDATE `articles` SET `qty`='"+ qtytotal+ "' where id='"+ a.getId() + "'";
					st = conn.createStatement();
					st.executeUpdate(sql1);
					
				}
				org.controlsfx.control.action.Action ad = Dialogs.create()
						.title("Paguaj Faturen")
						.actions(org.controlsfx.dialog.Dialog.ACTION_OK, org.controlsfx.dialog.Dialog.ACTION_NO)
						.message("Doni te Vazhdoni  Kete Veprim??? \n" + " Totali:" + tot.getText())
						.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
						.showConfirm();
				
				if(ad == org.controlsfx.dialog.Dialog.ACTION_OK){
					pst.execute();
					 pst.close();
						conn.close();
					 table.getItems().removeAll(articles);
					 Dialogs.create()
								.title("MESAZHI")
								.actions(org.controlsfx.dialog.Dialog.ACTION_CLOSE)
								.message("Fatura e blerjes u Ruaj me Sukses ")
								.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
								.showConfirm();
				}
				else
				{
					table.getItems().removeAll(articles);
				}
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void onEnter()
	{
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		article a = null;
		boolean found = false;
		String sql = "select * from articles where kodi_1='"+ kod.getText() + "' or kodi_2='"+ kod.getText() + "'";
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			if(rs.next())
			{
				found  =true;
				a = new article();
				art_id = rs.getInt(1);
				a.setId(rs.getInt(1));
				a.setKodi1(rs.getString(2));
				kod.setText(a.getKodi1());
				a.setPershkrimi(rs.getString(4));
				emri.setText(a.getPershkrimi());
				a.setNjesia(rs.getString(5));
				n = a.getNjesia();
				a.setPricein(rs.getDouble(11));
				blerje.setText(Double.toString(a.getPricein()));
			}
			else
			{
				 Dialogs.create()
					.title("Fature Blerje")
					.actions(org.controlsfx.dialog.Dialog.ACTION_CLOSE)
					.message("Artikulli nuk u gjend !!!")
					.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_CROSS_PLATFORM)
					.showConfirm();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	@FXML
	public void ZgjidhAction(ActionEvent event)
	{
		
		article a = new article();
		a.setId(art_id);
		a.setPershkrimi(emri.getText());
		a.setKodi1(kod.getText());
		a.setNjesia(n);
				if(!sasi.getText().isEmpty() && !blerje.getText().isEmpty())
				{
					a.setQuantity(Integer.parseInt(sasi.getText()));
					a.setPricein(Double.parseDouble(blerje.getText()));
					a.setTotali_artikull(a.getQuantity() * a.getPricein());
					articles.add(a);
					System.out.println(a.toString());
					System.out.println(articles.size());
				}
				System.out.println(articles.size());
			table.setItems(articles);

		double sum = 0;
		ArrayList<Double> columnData = new ArrayList<Double>(table.getItems().size());
		for(article item : table.getItems())
		{
			columnData.add(totali.getCellObservableValue(item).getValue());
			
		}
		for(int i = 0; i < columnData.size(); i++)
		{
			sum += columnData.get(i);
		}
		
		tot.setText(String.format( "%.2f", sum));
		kod.setText("");
		emri.setText("");
		blerje.setText("");
		sasi.setText("");
	}
		
	@FXML
	private void FshijAction(ActionEvent event) 
	{
		article a = table.getSelectionModel().getSelectedItem();
		articles.remove(a);
	}
	@FXML
	public void MbyllAction(ActionEvent event)
	{
		Stage stage = (Stage) mbyll.getScene().getWindow();
		stage.close();
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		id.setCellValueFactory(new PropertyValueFactory<article, Integer>("id"));
		kodi.setCellValueFactory(new PropertyValueFactory<article, String>("kodi1"));
		pershkrimi.setCellValueFactory(new PropertyValueFactory<article, String>("pershkrimi"));
		njesia.setCellValueFactory(new PropertyValueFactory<article, String>("njesia"));
		sasia.setCellValueFactory(new PropertyValueFactory<article, Integer>("quantity"));
		cmimi_blerje.setCellValueFactory(new PropertyValueFactory<article, Double>("pricein"));
		totali.setCellValueFactory(new PropertyValueFactory<article, Double>("totali_artikull"));
		
		kod.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(20));
		sasi.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(10));
		blerje.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(10));
		emri.addEventFilter(KeyEvent.KEY_TYPED, service.letter_Validation(30));

	}

}
