package Controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class mainController implements Initializable{

	@FXML
	private Label emeruser;
	
	@FXML
	private MenuBar mainmenu;
	
	@FXML
	private Menu homemenu;
	
	@FXML
	private MenuItem krijoartikull,krijokategori;
	
	@FXML
	private MenuItem modartikull,modkategori;
	
	@FXML
	private MenuItem krijoklient;
	
	@FXML
	private MenuItem modklient;
	
	@FXML
	private MenuItem krijopunetore;
	
	@FXML
	private MenuItem modpunetore;
	
	@FXML
	private MenuItem magazina;
	
	@FXML
	private MenuItem shitje;
	
	@FXML
	private MenuItem blerje;
	
	@FXML
	private MenuItem marzhi;
	
	@FXML
	private MenuItem artikullrap;
	
	@FXML
	private MenuItem shitjerap;
	
	@FXML
	private MenuItem rap_kompesim;
	
	
	@FXML
	private MenuItem blerjerap;
	
	@FXML
	private MenuItem menaxho;
	
	@FXML
	private MenuItem likujdo;
	
	@FXML
	private MenuItem gjenero;
	
	@FXML
	private MenuItem minimum;
	
	@FXML
	private MenuItem ndryshopass;
	
	//@FXML
	//private Menu about;
	
	@FXML
	private Button f_blerje, f_shitje, krijo_art, art_minimum, maga_zina, flete_hyrje, flete_dalje;
	
	int id_user  = 0;
	public void getUser(int id,String name,String surname)
	{
	
		emeruser.setText(name + " " + surname);
		id_user = id;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		krijoartikull.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/View/AddArtikull.fxml").openStream());
				Scene scene = new Scene(root,1000,600);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Krijo Artikull");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		krijo_art.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/View/AddArtikull.fxml").openStream());
				Scene scene = new Scene(root,1000,600);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Krijo Artikull");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		krijopunetore.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/View/AddPunetore.fxml").openStream());
				Scene scene = new Scene(root,1100,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Krijo Punetore");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		
		krijoklient.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/View/KrijoKlient.fxml").openStream());
				Scene scene = new Scene(root,1000,700);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Krijo Klient");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		ndryshopass.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/View/Change_pass.fxml").openStream());
				change_password change = (change_password)loader.getController();
				change.getUser(id_user, emeruser.getText());
				Scene scene = new Scene(root,600,400);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Ndrysho Password");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		
		minimum.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/View/Artikuj_Minimum.fxml").openStream());
				
				Scene scene = new Scene(root,1100,700);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Artikuj nen Minimum Klient");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		art_minimum.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/View/Artikuj_Minimum.fxml").openStream());
				
				Scene scene = new Scene(root,1100,700);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Artikuj nen Minimum Klient");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		modartikull.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Parent root = loader.load(getClass().getResource("/View/ModifikoArtikull.fxml").openStream());
				
				Scene scene = new Scene(root,1500,900);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Modifiko Artikuj");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		modklient.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/ModifikoClient.fxml").openStream());
				
				Scene scene = new Scene(root,1300,700);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Modifiko Klient");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		
		modpunetore.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/ModifikoPunetore.fxml").openStream());
				
				Scene scene = new Scene(root,1500,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Modifiko Punetore");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		
		krijokategori.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/KrijoKategori.fxml").openStream());
				
				Scene scene = new Scene(root,800,500);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Krijo Kategori");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		
		modkategori.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/ModifikoKategori.fxml").openStream());
				
				Scene scene = new Scene(root,1000,500);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Modifiko Kategori");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		magazina.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Magazina.fxml").openStream());
				
				Scene scene = new Scene(root,1500,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Magazina");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			
		});
		maga_zina.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Magazina.fxml").openStream());
				
				Scene scene = new Scene(root,1500,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Magazina");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			
		});
		
		marzhi.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Marzhi.fxml").openStream());
				
				Scene scene = new Scene(root,1500,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Marzhi");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		menaxho.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Menaxho_Faturat.fxml").openStream());
				
				Scene scene = new Scene(root,1500,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Menaxho Faturat");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		likujdo.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Likujdo_Kasierin.fxml").openStream());
				
				Scene scene = new Scene(root,1200,624);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Likujdo Kasierin");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		gjenero.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Gjenerim_Fatura.fxml").openStream());
				
				Scene scene = new Scene(root,1300,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Gjenero Faturat Permbledhese");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		shitje.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Shitje_tebera.fxml").openStream());
				
				Scene scene = new Scene(root,1500,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Shitjet");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		blerje.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Blerje_tebera.fxml").openStream());
				
				Scene scene = new Scene(root,1500,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Blerjet");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		f_blerje.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Fature_Blerje.fxml").openStream());
				
				Scene scene = new Scene(root,1500,900);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Fature Blerje");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		f_shitje.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Shitje_tebera.fxml").openStream());
				
				Scene scene = new Scene(root,1500,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Shitjet");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		flete_hyrje.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Flete_hyrje.fxml").openStream());
				
				Scene scene = new Scene(root,1500,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Flete Hyrje");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		flete_dalje.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Flete_dalje.fxml").openStream());
				
				Scene scene = new Scene(root,1500,800);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Flete Dalje");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		shitjerap.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Raporti Shitje.fxml").openStream());
				
				Scene scene = new Scene(root,800,400);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Flete Dalje");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		artikullrap.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Raport_Artikull.fxml").openStream());
				
				Scene scene = new Scene(root,800,400);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Flete Dalje");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		blerjerap.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Raporti Blerje.fxml").openStream());
				
				Scene scene = new Scene(root,800,400);
				primaryStage.setScene(scene);
				primaryStage.setTitle("Flete Dalje");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
		rap_kompesim.setOnAction(e->{
			try {
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				AnchorPane  root = loader.load(getClass().getResource("/View/Raport_kompesim.fxml").openStream());
				
				Scene scene = new Scene(root,550,350);
				primaryStage.setScene(scene);
				primaryStage.setTitle("LoginPage");
				primaryStage.show();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		});
	}

}
