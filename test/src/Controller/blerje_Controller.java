package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Model.article;
import Model.fornitor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class blerje_Controller implements Initializable{

	Database db = new Database();
	Service service = new Service();
	ObservableList<article> article;
	int art_id;
	int work_id;
	
	@FXML
	private TableView<article>	table;
	
	@FXML
	private TableColumn<article, Integer>id;
	
	@FXML
	private TableColumn<article, String>kodi;
	
	@FXML
	private TableColumn<article, String>pershkrimi;
	
	@FXML
	private TableColumn<article, String>njesia;
	
	@FXML
	private TableColumn<article, Integer>sasia;
	
	@FXML
	private TableColumn<article, Double>cmimi_blerje;
	
	@FXML
	private TableColumn<article, Double>totali;
	
	@FXML
	private DatePicker nga,deri;
	
	@FXML
	private Label tot, kodi_artikulli;
	
	@FXML
	private TextField artikulli;
	
	@FXML
	private ComboBox furnitor;
	
	@FXML
	private Button kerko, mbyll;
	
	public int SearchbyKodi(String str)
	{
		article a = null ;
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		String sql = "select * from articles where kodi_1='"+str+"' or kodi_2='"+str+"'";
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			if(rs.next())
			{
				art_id = rs.getInt("id");
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return art_id;
	}
	
	
	@FXML
	public void MbyllAction(ActionEvent event)
	{
		Stage stage = (Stage) mbyll.getScene().getWindow();
		stage.close();
	}
	@FXML
	public void KerkoAction(ActionEvent event)
	{
		String sql="";
		art_id = 0;
		work_id = 0;
		article = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		article a ;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		LocalDate ld1 = nga.getValue();
		LocalDate ld2 = deri.getValue();
		Date date1 = Date.valueOf(ld1);
		Date date2 = Date.valueOf(ld2);
		System.out.println("Kerko is pressed1");
		if(!furnitor.getSelectionModel().isEmpty()){
			String w = furnitor.getSelectionModel().getSelectedItem().toString();
					work_id = service.getfurnitors(w);
					System.out.println("Kerko is pressed 2" + " id" +work_id);
		}
		
		if(!artikulli.getText().isEmpty())
		{
			art_id = SearchbyKodi(artikulli.getText());
			System.out.println("Kerko is pressed 3" + " id" + art_id);
		}
		
		if(art_id != 0 && work_id != 0 )
		{
		
			sql = "select articles.id as id, articles.kodi_1 as kodi,description, qty,Njesia as njesia, sum(blerje.sasia) as sasia, price_in,sum(totali) as totali from blerje inner join articles on `article_id` = articles.id inner join fatura on fatura_id= fatura.id where  article_id='"+art_id+"'and furnitor_id='"+work_id+"' and create_at between '"+ date1 +"' and '" + date2 +"' group by (`article_id`)";
			try {
				st = conn.createStatement();
				rs = st.executeQuery(sql);
				
				while(rs.next())
				{
					System.out.println("Kerko is pressed 4");
					a = new article();
					a.setId(rs.getInt("id"));
					a.setKodi1(rs.getString("kodi"));
					a.setPershkrimi(rs.getString("description"));
					a.setNjesia(rs.getString("njesia"));
					a.setSasia(rs.getInt("sasia"));
					a.setPriceout(rs.getDouble("price_in"));
					a.setTotali_artikull(rs.getDouble("totali"));
					article.add(a);
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		else if(work_id != 0 && art_id == 0)
		{
			System.out.println("Kerko is pressed 5");
			sql = "select articles.id as id, articles.kodi_1 as kodi,description, qty,Njesia as njesia, sum(blerje.sasia) as sasia, price_in,sum(totali) as totali from blerje inner join articles on `article_id` = articles.id inner join fatura on fatura_id= fatura.id where  furnitor_id='"+work_id+"' and create_at between '"+ date1 +"' and '" + date2 +"' group by (`article_id`)";
			try {
				st = conn.createStatement();
				rs = st.executeQuery(sql);
				
				while(rs.next())
				{
					a = new article();
					a.setId(rs.getInt("id"));
					a.setKodi1(rs.getString("kodi"));
					a.setPershkrimi(rs.getString("description"));
					a.setNjesia(rs.getString("njesia"));
					a.setSasia(rs.getInt("sasia"));
					a.setPriceout(rs.getDouble("price_in"));
					a.setTotali_artikull(rs.getDouble("totali"));
					article.add(a);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (art_id != 0 && work_id == 0)
		{System.out.println("Kerko is pressed 6");
			sql = "select articles.id as id, articles.kodi_1 as kodi,description, qty,Njesia as njesia, sum(blerje.sasia) as sasia, price_in,sum(totali) as totali from blerje inner join articles on `article_id` = articles.id inner join fatura on fatura_id= fatura.id where  article_id='"+art_id+"' and   create_at between '"+ date1 +"' and '" + date2 +"' group by (`article_id`)";
			try {
				st = conn.createStatement();
				rs = st.executeQuery(sql);
				
				while(rs.next())
				{
					a = new article();
					a.setId(rs.getInt("id"));
					a.setKodi1(rs.getString("kodi"));
					a.setPershkrimi(rs.getString("description"));
					a.setNjesia(rs.getString("njesia"));
					a.setSasia(rs.getInt("sasia"));
					a.setPriceout(rs.getDouble("price_in"));
					a.setTotali_artikull(rs.getDouble("totali"));
					article.add(a);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			sql = "select articles.id as id, articles.kodi_1 as kodi,description, qty,Njesia as njesia, sum(blerje.sasia) as sasia, price_in,sum(totali) as totali from blerje inner join articles on `article_id` = articles.id inner join fatura on fatura_id= fatura.id where create_at between '"+ date1 +"' and '" + date2 +"' group by (`article_id`)";
			try {
				st = conn.createStatement();
				rs = st.executeQuery(sql);
				
				while(rs.next())
				{
					a = new article();
					a.setId(rs.getInt("id"));
					a.setKodi1(rs.getString("kodi"));
					a.setPershkrimi(rs.getString("description"));
					a.setNjesia(rs.getString("njesia"));
					a.setSasia(rs.getInt("sasia"));
					a.setPriceout(rs.getDouble("price_in"));
					a.setTotali_artikull(rs.getDouble("totali"));
					article.add(a);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		table.setItems(article);
		double sum = 0;
		ArrayList<Double> columnData = new ArrayList<Double>(table.getItems().size());
		for(article item : table.getItems())
		{
			columnData.add(totali.getCellObservableValue(item).getValue());
			
		}
		for(int i = 0; i < columnData.size(); i++)
		{
			sum += columnData.get(i);
		}
		
		tot.setText(String.format( "%.2f", sum));
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		id.setCellValueFactory(new PropertyValueFactory<article, Integer>("id"));
		kodi.setCellValueFactory(new PropertyValueFactory<article, String>("kodi1"));
		pershkrimi.setCellValueFactory(new PropertyValueFactory<article, String>("pershkrimi"));
		njesia.setCellValueFactory(new PropertyValueFactory<article, String>("njesia"));
		sasia.setCellValueFactory(new PropertyValueFactory<article, Integer>("sasia"));
		cmimi_blerje.setCellValueFactory(new PropertyValueFactory<article, Double>("pricein"));
		totali.setCellValueFactory(new PropertyValueFactory<article, Double>("totali_artikull"));
		
		table.setItems(article);
		artikulli.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(10));
		furnitor.setItems(service.getFornitor());
		
	}

}
