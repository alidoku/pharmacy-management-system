package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import Model.category;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class addKategoriController implements Initializable{

	Database db = new Database();
	Service service = new Service();
	
	@FXML
	private TextField emri;
	
	@FXML
	private CheckBox status;
	
	@FXML
	private TextArea persh;
	
	@FXML
	private Button ruaj,pastro,back;
	
	
	public void PastroAction(ActionEvent event)
	{
		emri.setText("");
		status.setSelected(true);
		persh.setText("");
		
	}
	@FXML
	public void BackAction(ActionEvent event)
	{
		Stage stage = (Stage) back.getScene().getWindow();
		
		stage.close();
	}
	@FXML
	private void RuajAction(ActionEvent event)
	{
		
		if(emri.getText().isEmpty())
		{
			ruaj.disabledProperty();
			Dialogs.create()
			.title("MESAZHI")
			.message("Ju lutem plotesojini te gjithe fushat !!! ")
			.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
			.showWarning();
			return;
		}
		String sql = "INSERT INTO `categories`(`id`, `name`, `status`, `description`) VALUES (?,?,?,?)";
		Connection conn = db.connect();
		String sts;
		if(status.isSelected())
			sts = "AKTIV";
		else
			sts = "PASIV";
		
		category c = new category();
		c.setName(emri.getText());
		c.setStatus(sts);
		c.setDescription(persh.getText());
		
		try {
			PreparedStatement pst = (PreparedStatement)conn.prepareStatement(sql);
			 int id = 0;
			 pst.setInt(1, id);
			 pst.setString(2, c.getName());
			 pst.setString(3, c.getStatus());
			 pst.setString(4, c.getDescription());
			 
			 pst.execute();
			 pst.close();
			 conn.close();
			 Dialogs.create()
				.title("MESAZHI")
				.message("Kategoria u ruajt me sukse !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showInformation();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Stage stage = (Stage) ruaj.getScene().getWindow();
		
		stage.close();
	}
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

}
