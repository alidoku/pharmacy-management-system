package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import Model.article;
import Model.client;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class modifiko_Klient implements Initializable{

	Database db = new Database();
	Service service = new Service();
	
	@FXML
	private TableView <client> table;
	
	@FXML
	private TableColumn <client, Integer> id;
	@FXML
	private TableColumn <client, String> emri;
	@FXML
	private TableColumn <client, String> mbiemri;
	@FXML
	private TableColumn <client, String> address;
	@FXML
	private TableColumn <client, String> email;
	@FXML
	private TableColumn <client, Integer> telefoni;
	@FXML
	private TableColumn <client, String> kategoria;
	@FXML
	private TableColumn <client, String> pershkrimi;
	@FXML
	private TableColumn <client, String> status;
	@FXML
	private TableColumn <client, Date> date_regj;
	
	@FXML
	private TextField em,mb,ad,ema,te;
	
	@FXML
	private TextArea pe;
	
	@FXML
	private ChoiceBox ka;
	
	@FXML
	private CheckBox st;
	@FXML
	private Button ruaj, pastro, back;
	@FXML
	private Label label;
	
	public ObservableList<client> getClients()
	{
		ObservableList<client> client = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		client c;
		String kateg = "Jo Kategori";
		
		
		String sql = "select * from clients";
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				kateg = service.getkategoria(rs.getInt(7));
				String num_tel = rs.getBigDecimal(6).toString();
				System.out.println(kateg);
				c = new client(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),num_tel, kateg, rs.getString(8),rs.getString(9),rs.getDate(10));
				client.add(c);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return client;
	}
	public void PastroAction(ActionEvent event)
	{
		em.setText("");
		mb.setText("");
		ad.setText("");
		ema.setText("");
		te.setText("");
		ka.getSelectionModel().select(0);
		pe.setText("");
	}
	@FXML
	public void BackAction(ActionEvent event)
	{
		Stage stage = (Stage) back.getScene().getWindow();
		
		stage.close();
	}
	@FXML
	private void RuajAction(ActionEvent event)
	{
		Connection conn = db.connect();
		Statement smt;
		 if(em.getText().isEmpty() ||mb.getText().isEmpty() || !st.isSelected() )
			{
			 ruaj.disabledProperty();
			 Dialogs.create()
				.title("MESAZHI")
				.message("Ju lutem plotesojini te gjithe fushat !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showWarning();
				return;
			}
		String sts;
		String k = ka.getSelectionModel().getSelectedItem().toString();
		int kat_id = service.getkategori(k);
		
		
		if(st.isSelected())
			sts = "AKTIV";
		else
			sts = "PASIV";
		
		String sql = "UPDATE `clients` SET `name`='"+em.getText()+"',`surname`='"+mb.getText()+"',`address`='"+ad.getText()+"',`email`='"+em.getText()+"',`phone_num`='"+te.getText()+"',`categori_id`='"+kat_id+"',`description`='"+pe.getText()+"',`status`='"+sts+"' WHERE id='"+label.getText()+"'";
		
		try {
			smt = conn.createStatement();
			smt.executeUpdate(sql);
			
			Dialogs.create()
			.title("MESAZHI")
			.message("Klienti u modifikua me sukses !!! ")
			.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
			.showWarning();		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		table.getItems().clear();
		table.setItems(getClients());
		
	}
	@FXML
	public void MouseEvent(javafx.scene.input.MouseEvent event)
	{
		if(event.isPrimaryButtonDown() && event.getClickCount() == 2){
		client c = (client) table.getSelectionModel().getSelectedItem();
		em.setText(c.getName());
		mb.setText(c.getSurname());
		ad.setText(c.getAddress());
		ema.setText(c.getEmail());
		te.setText(c.getNr_cel());
		ka.getSelectionModel().select(c.getCategory());
		pe.setText(c.getPershkrimi());
		if(c.getStatus().equals("AKTIV"))
			st.setSelected(true);
		else
			st.setSelected(false);
		label.setText(Integer.toString(c.getId()));	
		}
	
		
	}
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		id.setCellValueFactory(new PropertyValueFactory<client, Integer>("id"));
		emri.setCellValueFactory(new PropertyValueFactory<client, String>("name"));
		mbiemri.setCellValueFactory(new PropertyValueFactory<client, String>("surname"));
		address.setCellValueFactory(new PropertyValueFactory<client, String>("address"));
		email.setCellValueFactory(new PropertyValueFactory<client, String>("email"));
		telefoni.setCellValueFactory(new PropertyValueFactory<client, Integer>("nr_cel"));
		kategoria.setCellValueFactory(new PropertyValueFactory<client, String>("category"));
		pershkrimi.setCellValueFactory(new PropertyValueFactory<client, String>("pershkrimi"));
		status.setCellValueFactory(new PropertyValueFactory<client, String>("status"));
		date_regj.setCellValueFactory(new PropertyValueFactory<client, Date>("created_at"));
		
		table.setItems(getClients());
		
		te.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(15));
		em.addEventFilter(KeyEvent.KEY_TYPED, service.letter_Validation(30));
		mb.addEventFilter(KeyEvent.KEY_TYPED, service.letter_Validation(30));
		
		ResultSet rs = service.getcategories();
		ObservableList<String> zgjedhje = FXCollections.observableArrayList();
		try {
			while(rs.next()){
			zgjedhje.add(rs.getString(2));
			ka.setItems(zgjedhje);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
