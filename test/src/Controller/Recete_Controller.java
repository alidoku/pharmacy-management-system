package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import Model.category;
import Model.client;
import Model.presctiption;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Recete_Controller implements Initializable{
	
	Database db = new Database();
	Service service = new Service();
	ObservableList<String> ilace = FXCollections.observableArrayList();
	private int client_id;
	@FXML
	private TextField id,klienti,Doktorri,spitali;
	
	@FXML
	private ComboBox ilacet;
	
	@FXML
	private TextArea komenti;
	
	@FXML
	private ListView list;
	
	@FXML
	private Button ruaj, pastro;
	
	@FXML
	public void PastroAction(ActionEvent event)
	{
		id.setText("");
		Doktorri.setText("");
		spitali.setText("");
		list.setItems(null);
		komenti.setText("");
		
	}
	@FXML
	public void RuajAction(ActionEvent event)
	{
		if(id.getText().isEmpty() && Doktorri.getText().isEmpty() && spitali.getText().isEmpty() && komenti.getText().isEmpty() &&list.getItems().isEmpty())
		{
			ruaj.disabledProperty();
			JOptionPane.showMessageDialog(null, "JU lutem plotesojini te gjitha fushat ");
			return;
		}
		String sql = "INSERT INTO `recipies`(`id`, `client_id`, `doktor`, `spitali`, `ilacet`, `pershkrimi`) VALUES (?,?,?,?,?,?)";
		Connection conn = db.connect();
		String dose = "";
		for(int i = 0; i < list.getItems().size();i++)
			dose += list.getItems().get(i)+" ";
		
		client c = new client();
		
		
		c.setId(client_id);
		c.setName(klienti.getText());
		
		
		presctiption p = new presctiption(Integer.parseInt(id.getText()),c,komenti.getText(),dose, Doktorri.getText(),spitali.getText());
		
		
		try {
			PreparedStatement pst = (PreparedStatement)conn.prepareStatement(sql);
			
			 pst.setInt(1, p.getId());
			 pst.setInt(2, c.getId());
			 pst.setString(3, p.getDoktor());
			 pst.setString(4, p.getSpitali());
			 pst.setString(5, p.getMedicine());
			 pst.setString(6, p.getNote());
			 
			 pst.execute();
			 pst.close();
			 conn.close();
			 JOptionPane.showMessageDialog(null, "Receta u ruajt me sukses");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Stage stage = (Stage) ruaj.getScene().getWindow();
		
		stage.close();
		
	}
	
	public void getKlient(String name)
	{
		klienti.setText(name);
	}
	public void getClientId(int id)
	{
		client_id = id;
	}
	@FXML
	public void Selected(Event e)
	{
		 String item = ilacet.getSelectionModel().getSelectedItem().toString();
		 ilace.add(item);
		 list.getItems().add(item);
		
	}
	@FXML
	public void MouseEvent(javafx.scene.input.MouseEvent event)
	{
		if(event.isPrimaryButtonDown() && event.getClickCount() == 2){
			Object object = list.getSelectionModel().getSelectedItem();
			list.getItems().remove(object);
			
		}
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		ResultSet rs = service.getArticles();
		ObservableList<String> lst = FXCollections.observableArrayList();
		try {
			while(rs.next())
			{
				lst.add(rs.getString(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		{
			ilacet.setItems(lst);
			
		}
	}

}
