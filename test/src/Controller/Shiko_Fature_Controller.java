package Controller;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

import Model.Bills;
import Model.Invoice;
import Model.article;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class Shiko_Fature_Controller implements Initializable{

	Database db = new Database();
	Service service = new Service();
	ObservableList<article> articles = FXCollections.observableArrayList();
	
	@FXML
	private TableView<article> table;
	
	@FXML
	private TableColumn<article, Integer> id;
	
	@FXML
	private TableColumn<article, String> kodi;
	
	@FXML
	private TableColumn<article, String> emri;
	
	@FXML
	private TableColumn<article, String> njesia;
	
	@FXML
	private TableColumn<article, Double> sasia;
	
	@FXML
	private TableColumn<article, Double> cmimi_shitjes;
	
	@FXML
	private TableColumn<article, Double> totali;
	
	@FXML
	private Label tot,table_id, total_sum;
	
	public void getTable(int id, double totali)
	{
	
		ObservableList<Invoice> invoice = service.getFatura(id);
		article article = new article();
		for(int i = 0; i < invoice.size(); i++)
		{
			Invoice in  = invoice.get(i);
			article = service.getArtikuj(in.getArtikull(), in.getSasia(), in.getTotali());
			
			articles.add(article);
		}
		tot.setText(Double.toString(totali));
		
	}
	public ObservableList<article> generate()
	{
		
		ObservableList<Invoice> invoice = service.getFatura(100);
		article article = new article();
		for(int i = 0; i < invoice.size(); i++)
		{
			Invoice in  = invoice.get(i);
			article = service.getArtikuj(in.getArtikull(), in.getSasia(), in.getTotali());
			
			articles.add(article);
		}
		
		return articles;
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		articles = FXCollections.observableArrayList();
		id.setCellValueFactory(new PropertyValueFactory<article, Integer>("id"));
		kodi.setCellValueFactory(new PropertyValueFactory<article, String>("kodi1"));
		emri.setCellValueFactory(new PropertyValueFactory<article, String>("pershkrimi"));
		njesia.setCellValueFactory(new PropertyValueFactory<article, String>("njesia"));
		sasia.setCellValueFactory(new PropertyValueFactory<article, Double>("sasia"));
		cmimi_shitjes.setCellValueFactory(new PropertyValueFactory<article, Double>("priceout"));
		totali.setCellValueFactory(new PropertyValueFactory<article, Double>("totali_artikull"));
		
			table.setItems(articles);
			
		tot.setText(total_sum.getText());
	}

}
