package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import com.sun.deploy.uitoolkit.impl.fx.ui.FXConsole;

import Model.article;
import Model.worker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class Likujdo_Kasierin_Controller implements Initializable{

	Database db = new Database();
	Service service = new Service();
	ObservableList<worker> workers;
	
	@FXML
	private TableView<worker> table;
	
	@FXML
	private TableColumn<worker, String> pika_shitjes;
	
	@FXML
	private TableColumn<worker, String> kodi;
	
	@FXML
	private TableColumn<worker, String> kasieri;
	
	@FXML
	private TableColumn<worker, Double> xhiro;
	
	@FXML
	private TableColumn<worker, Double> paguar;
	
	@FXML
	private Button zgjedhur, gjithe, mbyll;
	
	public ObservableList<worker> getWorker()
	{
		 workers = FXCollections.observableArrayList();
		String sql = "SELECT users.id as id, concat(users.name, users.surname) as emri, sum(`total`) as total, sum(`total`) as paguar FROM fatura LEFT JOIN users ON fatura.worker_id=users.id where fatura.status='PRERE' group by worker_id";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		worker worker = null;
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				worker = new worker();
				worker.setSurname("Pharmacy EAGLY");
				worker.setId(rs.getInt("id"));
				worker.setName(rs.getString("emri"));
				worker.setShenime(Double.toString(rs.getDouble("total")));
				workers.add(worker);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return workers;
		
	}
	@FXML
	private void KasieriZgjedhur(ActionEvent event)
	{
		worker w = table.getSelectionModel().getSelectedItem();
		String sql = "UPDATE `fatura` SET `status`='LIKUJDUAR' WHERE worker_id='"+w.getId()+"' and status='PRERE'";
		Connection conn = db.connect();
		Statement smt;
		try {
			smt = conn.createStatement();
			smt.executeUpdate(sql);
			
			 Dialogs.create()
					.title("Likujdo Kasierin")
					.actions(org.controlsfx.dialog.Dialog.ACTION_CLOSE)
					.message("Kasieri U likujdua me Sukses")
					.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
					.showConfirm();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		workers.remove(w);
		table.setItems(workers);
	}
	@FXML
	private void GjitheKasieret(ActionEvent event)
	{
		ObservableList<worker> w = table.getItems();
		String sql = "UPDATE `fatura` SET `status`='LIKUJDUAR' WHERE 1";
		Connection conn = db.connect();
		Statement smt;
		try {
			
			smt = conn.createStatement();
			smt.executeUpdate(sql);
			
			 Dialogs.create()
					.title("Likujdo Kasierin")
					.actions(org.controlsfx.dialog.Dialog.ACTION_CLOSE)
					.message("Kasieret  U likujduan me Sukses")
					.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
					.showConfirm();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int i = 0; i < workers.size(); i++)
			workers.remove(i);
		table.setItems(workers);
	}
	@FXML
	private void MbyllAction(ActionEvent event)
	{
		Stage stage = (Stage) mbyll.getScene().getWindow();
		
		stage.close();
	}
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		pika_shitjes.setCellValueFactory(new PropertyValueFactory<worker, String>("surname"));
		kodi.setCellValueFactory(new PropertyValueFactory<worker, String>("id"));
		kasieri.setCellValueFactory(new PropertyValueFactory<worker, String>("name"));
		xhiro.setCellValueFactory(new PropertyValueFactory<worker, Double>("shenime"));
		paguar.setCellValueFactory(new PropertyValueFactory<worker, Double>("shenime"));
		
		
		table.setItems(getWorker());
		
	}

}
