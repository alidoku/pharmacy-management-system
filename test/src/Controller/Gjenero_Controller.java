package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import Model.article;
import Model.worker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class Gjenero_Controller implements Initializable{

	Database db = new Database();
	Service service = new Service();
	ObservableList<article>  article;
	
	@FXML
	private TableView<article> table;
	
	@FXML
	private TableColumn<article, Integer> id;
	
	@FXML
	private TableColumn<article, String> kodi;
	
	@FXML
	private TableColumn<article, String> pershkrimi;
	
	@FXML
	private TableColumn<article, Integer> gjendje_para;
	
	@FXML
	private TableColumn<article, Integer> shitur;
	
	@FXML
	private TableColumn<article, Integer> gjendje_pas;
	
	@FXML
	private Button gjenero,mbyll;
	
	
	public ObservableList<article> getArticles()
	{
		 article = FXCollections.observableArrayList();
		 String sql = "select articles.id as id, articles.kodi_1 as kodi,description, qty, sum(shitje.sasia) as sasia from shitje inner join articles on `artikull_id` = articles.id where likujdim = 'PRERE' group by (`artikull_id`)";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		article a = null;
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				a = new article();
				a.setId(rs.getInt("id"));
				a.setKodi1(rs.getString("kodi"));
				a.setPershkrimi(rs.getString("description"));
				a.setQuantity(rs.getInt("qty"));
				a.setSasia(rs.getInt("sasia"));
				int t = (int) (a.getQuantity() - a.getSasia());
				a.setTotali_artikull(t);
				article.add(a);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return article;
		
	}
	
	@FXML
	private void MbyllAction(ActionEvent event)
	{
		Stage stage = (Stage) mbyll.getScene().getWindow();
		
		stage.close();
	}
	public int FaturaId()
	{
		String sql = "SELECT  *  FROM fatura";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		int result = 0;
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next())
				result = rs.getInt(1);
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result+1;
		
	}
	
	public int findArtikull(int id)
	{
		String sql = "select * from articles where id='"+id + "'";
		Connection conn = db.connect();
		Statement st;
		ResultSet rs;
		int quantity = 0;
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			if(rs.next())
				quantity = rs.getInt(9);
			else
				quantity = 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return quantity;
	
	}
	
	@FXML
	private void GjeneroAction(ActionEvent event)
	{
		String sql = "UPDATE `shitje` SET `likujdim`='GJENERUAR' WHERE 1";
		String sql1 = "INSERT INTO `fatura`(`id`, `emer`, `Tipi`, `worker_id`,`client_id`,`total`,`status`)"+ " VALUES (?,?,?,?,?,?,?)";
		Connection conn = db.connect();
		Statement st;
		Statement smt;
		int cnt = FaturaId();
		System.out.println(cnt);
		try {
			smt = conn.createStatement();
			smt.executeUpdate(sql);
			
					PreparedStatement pst = (PreparedStatement)conn.prepareStatement(sql1);
					pst.setInt(1, cnt);
					pst.setString(2, "FATURE GJENERUERE");
					pst.setString(3, "GJENERUESE");
					pst.setInt(4, 1);
					pst.setInt(5, 1);
					pst.setDouble(6, 1);
					pst.setString(7, "LIKUJDUAR");
					pst.execute();
					pst.close();
					
					for(int i = 0; i < article.size();i++)
					{
						article a = article.get(i);
						
						int qty = findArtikull(a.getId());
						int qtytotal = (int) (qty + a.getSasia());
						String sql2 = "UPDATE `articles` SET `qty`='"+ qtytotal+ "' where id='"+ a.getId() + "'";
						st = conn.createStatement();
						st.executeUpdate(sql2);
						
					}
			
			 Dialogs.create()
					.title("Gjenero Faturat")
					.actions(org.controlsfx.dialog.Dialog.ACTION_CLOSE)
					.message("Fatura u gjenerua me sukses")
					.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
					.showConfirm();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		for(int i = 0; i < article.size();i++)
			article.remove(i);
		table.setItems(article);
		
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		id.setCellValueFactory(new PropertyValueFactory<article, Integer>("id"));
		kodi.setCellValueFactory(new PropertyValueFactory<article, String>("kodi1"));
		pershkrimi.setCellValueFactory(new PropertyValueFactory<article, String>("pershkrimi"));
		gjendje_para.setCellValueFactory(new PropertyValueFactory<article, Integer>("quantity"));
		shitur.setCellValueFactory(new PropertyValueFactory<article, Integer>("sasia"));
		gjendje_pas.setCellValueFactory(new PropertyValueFactory<article, Integer>("totali_artikull"));
		table.setItems(getArticles());
		
	}

}
