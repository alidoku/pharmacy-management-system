/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author user
 */
public class goods {
    
    private int id;
    private String type;
    private double quantity;
    private Date out_date;
    private double price_in;
    private double price_out;
    String kodi;

    public goods() {
        id = 0;
        type = null;
        quantity = 0.0;
        out_date = new Date();
        price_in = 0.0;
        price_out = 0.0;
    }

    public goods(int id, String type, double quantity, Date out_date, double price_in, double price_out, String kodi) {
        this.id = id;
        this.type = type;
        this.quantity = quantity;
        this.out_date = out_date;
        this.price_in = price_in;
        this.price_out = price_out;
        this.kodi = kodi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Date getOut_date() {
        return out_date;
    }

    public void setOut_date(Date out_date) {
        this.out_date = out_date;
    }

    public double getPrice_in() {
        return price_in;
    }

    public void setPrice_in(double price_in) {
        this.price_in = price_in;
    }

    public double getPrice_out() {
        return price_out;
    }

    public void setPrice_out(double price_out) {
        this.price_out = price_out;
    }

    public String getKodi() {
        return kodi;
    }

    public void setKodi(String kodi) {
        this.kodi = kodi;
    }
    
    
    
    
    
}
