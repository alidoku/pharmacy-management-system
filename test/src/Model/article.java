/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author user
 */
public class article {

	private int id;
    private String kodi1;
    private String kodi2;
    private String pershkrimi;
    private String njesia;
    private int min;
    private String kategoria;
    private String kompesim;
    private int quantity;
    private String status;
    private double pricein;
    private double priceout;
    private Date enter_date;
    private double marzhi;
    private double sasia;
    private double totali_artikull;
   
  
    public article(int id, String kodi1, String kodi2, String pershkrimi, String njesia, int min, String kategoria,
			String kompesim, int quantity, String status, double pricein, double priceout) {
		super();
		this.id = id;
		this.kodi1 = kodi1;
		this.kodi2 = kodi2;
		this.pershkrimi = pershkrimi;
		this.njesia = njesia;
		this.min = min;
		this.kategoria = kategoria;
		this.kompesim = kompesim;
		this.quantity = quantity;
		this.status = status;
		this.pricein = pricein;
		this.priceout = priceout;
		this.enter_date = enter_date;
		
	}

	public article(int id,String kod1, String pershkrimi, String njesia, int min,double pricein, int qty)
    {
    	this.id = id;
    	this.kodi1 = kod1;
    	this.pershkrimi = pershkrimi;
    	this.njesia = njesia;
    	this.min = min;
    	this.pricein = pricein;
    	this.quantity = qty;
    	
    }

    public article() {
        this.kodi1 = null;
        this.kodi2 = null;
        this.pershkrimi = null;
        this.njesia = null;
        this.min = 0;
        this.status = null;
        this.kompesim = null;
        this.kategoria = null;
        this.enter_date = new Date();
        this.quantity = 0;
        this.pricein = 0.0;
        this.priceout = 0.0;
        this.marzhi = 0.0;
      
    }

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKodi1() {
        return kodi1;
    }

    public void setKodi1(String kodi1) {
        this.kodi1 = kodi1;
    }

    public String getKodi2() {
        return kodi2;
    }

    public void setKodi2(String kodi2) {
        this.kodi2 = kodi2;
    }

    public String getPershkrimi() {
        return pershkrimi;
    }

    public void setPershkrimi(String pershkrimi) {
        this.pershkrimi = pershkrimi;
    }

    public String getNjesia() {
        return njesia;
    }

    public void setNjesia(String njesia) {
        this.njesia = njesia;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKompesim() {
        return kompesim;
    }

    public void setKompesim(String kompesim) {
        this.kompesim = kompesim;
    }

    public String getKategoria() {
        return kategoria;
    }

    public void setKategoria(String kategoria) {
        this.kategoria = kategoria;
    }
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPriceout() {
        return priceout;
    }

    public void setPriceout(double priceout) {
        this.priceout = priceout;
    }
      public Date getEnter_date() {
        return enter_date;
    }

    public void setEnter_date(Date enter_date) {
        this.enter_date = enter_date;
    }

    public double getPricein() {
        return pricein;
    }

    public void setPricein(double pricein) {
        this.pricein = pricein;
    }

	public Double getMarzhi() {
		return marzhi;
	}

	public void setMarzhi(double marzhi) {
		this.marzhi = marzhi;
	}

	public double getSasia() {
		return sasia;
	}

	public void setSasia(double sasia) {
		this.sasia = sasia;
	}

	public double getTotali_artikull() {
		return totali_artikull;
	}

	public void setTotali_artikull(double totali_artikull) {
		this.totali_artikull = totali_artikull;
	}
    

}
