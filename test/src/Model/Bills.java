package Model;

public class Bills {
	
	private int id;
	private String emer;
	private String tipi;
	private  int worker_id;
	private double total;
	private int client_id;
	private String status;
	private String created_at;
	public Bills(int id, String emer, String tipi, int worker_id, int client,double total,String sts) {
		super();
		this.id = id;
		this.emer = emer;
		this.tipi = tipi;
		this.worker_id = worker_id;
		this.total = total;
		this.status = sts;
		this.client_id = client;
	}
	
	public Bills() {
		super();
		this.id = 0;
		this.emer = null;
		this.tipi = null;
		this.worker_id = 0;
		this.total = 0;
		this.status = null;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmer() {
		return emer;
	}
	public void setEmer(String emer) {
		this.emer = emer;
	}
	public String getTipi() {
		return tipi;
	}
	public void setTipi(String tipi) {
		this.tipi = tipi;
	}
	public int getWorker_id() {
		return worker_id;
	}
	public void setWorker_id(int worker_id) {
		this.worker_id = worker_id;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getClient_id() {
		return client_id;
	}

	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}
	
	

}
