package Model;

import java.util.Date;

public class category {

	
	private int id;
	private String name;
	private String status;
	private String description;
	private Date created_at;
	public category(int id, String name, String status, String description, Date created_at) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
		this.description = description;
		this.created_at = created_at;
	}
	public category() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	
	
	
	
}
