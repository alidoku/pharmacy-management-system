/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author user
 */
public class client {
    
    private int id;
    private String  name;
    private String surname;
    private String address;
    private String email;
    private String nr_cel;
    private String category;
    private String pershkrimi;
    private String status;
    private Date created_at;

   

	public client(int id, String name, String surname, String address, String email, String nr_cel, String category,
			String pershkrimi, String status, Date created_at) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.address = address;
		this.email = email;
		this.nr_cel = nr_cel;
		this.category = category;
		this.pershkrimi = pershkrimi;
		this.status = status;
		this.created_at = created_at;
	}

	public client() {
        id = 0;
        name = null;
        surname = null;
        address = null;
        email = null;
        nr_cel = null;
        status = null;
        category = null;
        pershkrimi = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNr_cel() {
        return nr_cel;
    }

    public void setNr_cel(String nr_cel) {
        this.nr_cel = nr_cel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPershkrimi() {
        return pershkrimi;
    }

    public void setPershkrimi(String pershkrimi) {
        this.pershkrimi = pershkrimi;
    }
    public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
    
    
    
}
