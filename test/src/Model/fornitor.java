/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class fornitor {
    
    private int id;
    private String name;
    private double blerje;
    private String status;
    private String address;
    

      public fornitor(int id, String name, double blerje,String s, String add) {
        this.id = id;
        this.name = name;
        this.blerje = blerje;
        this.status = s;
        this.address = add;
    }

    public fornitor() {
        id = 0;
        name = null;
        blerje = 0.0;
        status = null;
        address = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBlerje() {
        return blerje;
    }

    public void setBlerje(double blerje) {
        this.blerje = blerje;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
      
      
    
    
}
